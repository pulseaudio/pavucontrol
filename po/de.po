# German translation of pavucontrol
# Copyright (C) 2008 pavucontrol
# This file is distributed under the same license as the pavucontrol package.
# Fabian Affolter <fab@fedoraproject.org>, 2008.
# Florian Steinel <fsteinel@fedoraproject.org>, 2008
# Tobias Weise <tobias.weise@web.de>, 2020.
# Fabian Affolter <mail@fabian-affolter.ch>, 2020.
# Ettore Atalan <atalanttore@googlemail.com>, 2021.
# Joachim Philipp <joachim.philipp@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: pavucontrol\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-24 17:04-0400\n"
"PO-Revision-Date: 2022-07-22 08:19+0000\n"
"Last-Translator: Joachim Philipp <joachim.philipp@gmail.com>\n"
"Language-Team: German <https://translate.fedoraproject.org/projects/"
"pulseaudio/pavucontrol/de/>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.13\n"
"X-Poedit-Language: German\n"

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:7
#: src/org.pulseaudio.pavucontrol.desktop.in:5 src/mainwindow.ui:5
msgid "Volume Control"
msgstr "Lautstärkeregler"

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:8
msgid "Adjust device and app volumes"
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:9
#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:11
msgid "The PulseAudio Developers"
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:14
msgid ""
"PulseAudio Volume Control (pavucontrol) is a volume control tool (“mixer”) "
"for the PulseAudio sound server. In contrast to classic mixer tools, this "
"one allows you to control both the volume of hardware devices and of each "
"playback stream separately."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:21
msgid "The “Playback” tab"
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:25
msgid "The “Recording” tab"
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:29
msgid "The “Output” tab"
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:33
msgid "The “Input” tab"
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:37
#, fuzzy
msgid "The “Configuration” tab"
msgstr "_Konfiguration"

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:45
msgid "Migrate from Gtk 3 to 4."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:46
msgid "Embed UI resources in executable."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:47
msgid "Rename \"Set as fallback\" to \"Default\" for better legibility."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:48
msgid "Support 144 Hz monitors with level bars."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:49
msgid ""
"App icons will fallback to generic rather than missing image, and this will "
"be more common with Gtk 4."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:51
msgid "Lots of translation updates."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:52
msgid "Drop autotools build in favour of meson."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:53
msgid "Make libcanberra dependency optional."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:60
msgid "Support for switching Bluetooth codecs (new in PulseAudio 15.0)."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:61
msgid ""
"Support for locking card profiles (new in PulseAudio 15.0). Locking a "
"profile prevents PulseAudio from automatically switching away from that "
"profile on plug/unplug events."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:62
msgid ""
"New translations: Asturian, Basque, Belarusian, Galician, Hebrew, Kazakh, "
"Norwegian Bokmål, Sinhala, Slovenian"
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:63
msgid ""
"Updated translations: Catalan, Chinese (Simplified), Chinese (Traditional), "
"Croatian, Danish, Dutch, Finnish, French, German, Hungarian, Italian, "
"Japanese, Korean, Lithuanian, Norwegian Nynorsk, Polish, Portugese, "
"Portugese (Brazil), Slovak, Spanish, Swedish, Turkish, Ukrainian."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:70
msgid ""
"There can now be only one pavucontrol window open at a time. Trying to start "
"pavucontrol for a second time brings the first window to foreground."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:71
msgid ""
"Added a \"Show volume meters\" checkbox to the Configuration tab. Disabling "
"the volume meters reduces CPU use."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:72
msgid "Improve the use of space (remove useless margins and paddings)."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:73
msgid "Use a more appropriate icon for the channel lock button."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:74
msgid ""
"Better channel label layout, prevents volume sliders from getting unaligned."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:75
msgid ""
"Maximum latency offset increased from 2 to 5 seconds to accommodate AirPlay "
"devices that often have higher latency than 2 seconds (this is not that "
"useful on newer PulseAudio versions, though, because the latency is reported "
"much more accurately than before)."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:76
msgid "New --version command line option."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:77
msgid ""
"New translations: Chinese (Taiwan), Croatian, Korean, Norwegian Nynorsk, "
"Lithuanian, Valencian."
msgstr ""

#: src/org.pulseaudio.pavucontrol.metainfo.xml.in:78
msgid ""
"Updated translations: Finnish, French, German, Italian, Japanese, Polish, "
"Swedish."
msgstr ""

#: src/org.pulseaudio.pavucontrol.desktop.in:4 src/pavucontrol.cc:877
msgid "PulseAudio Volume Control"
msgstr "PulseAudio-Lautstärkeregler"

#: src/org.pulseaudio.pavucontrol.desktop.in:6
msgid "Adjust the volume level"
msgstr "Lautstärke anpassen"

#: src/org.pulseaudio.pavucontrol.desktop.in:12
msgid ""
"pavucontrol;Microphone;Volume;Fade;Balance;Headset;Speakers;Headphones;Audio;"
"Mixer;Output;Input;Devices;Playback;Recording;System Sounds;Sound Card;"
"Settings;Preferences;"
msgstr ""

#: src/mainwindow.ui:40
msgid "<i>No application is currently playing audio.</i>"
msgstr "<i>Keine Applikation gibt zur Zeit Ton wieder.</i>"

#: src/mainwindow.ui:66 src/mainwindow.ui:157
msgid "<b>_Show:</b>"
msgstr "<b>_Anzeigen:</b>"

#: src/mainwindow.ui:82 src/mainwindow.ui:173
msgid "All Streams"
msgstr "Alle Streams"

#: src/mainwindow.ui:85 src/mainwindow.ui:176
msgid "Applications"
msgstr "Anwendungen"

#: src/mainwindow.ui:88 src/mainwindow.ui:179
msgid "Virtual Streams"
msgstr "Virtuelle Streams"

#: src/mainwindow.ui:107
msgid "_Playback"
msgstr "_Wiedergabe"

#: src/mainwindow.ui:131
msgid "<i>No application is currently recording audio.</i>"
msgstr "<i>Keine Applikation nimmt zur Zeit Ton auf.</i>"

#: src/mainwindow.ui:198
msgid "_Recording"
msgstr "_Aufnahme"

#: src/mainwindow.ui:222
msgid "<i>No output devices available</i>"
msgstr "<i>Keine Ausgabegeräte verfügbar</i>"

#: src/mainwindow.ui:248
msgid "<b>S_how:</b>"
msgstr "<b>A_nzeigen:</b>"

#: src/mainwindow.ui:265
msgid "All Output Devices"
msgstr "Alle Ausgabegeräte"

#: src/mainwindow.ui:268
msgid "Hardware Output Devices"
msgstr "Hardware Ausgabegeräte"

#: src/mainwindow.ui:271
msgid "Virtual Output Devices"
msgstr "Virtuelle Ausgabegeräte"

#: src/mainwindow.ui:290
msgid "_Output Devices"
msgstr "A_usgabegeräte"

#: src/mainwindow.ui:314
msgid "<i>No input devices available</i>"
msgstr "<i>Keine Eingabegeräte verfügbar.</i>"

#: src/mainwindow.ui:340
msgid "<b>Sho_w:</b>"
msgstr "<b>Anze_igen:</b>"

#: src/mainwindow.ui:357
msgid "All Input Devices"
msgstr "Alle Eingabegeräte"

#: src/mainwindow.ui:360
msgid "All Except Monitors"
msgstr "Alle außer Monitore"

#: src/mainwindow.ui:363
msgid "Hardware Input Devices"
msgstr "Hardware Eingabegeräte"

#: src/mainwindow.ui:366
msgid "Virtual Input Devices"
msgstr "Virtuelle Eingabegeräte"

#: src/mainwindow.ui:369
msgid "Monitors"
msgstr "Monitore"

#: src/mainwindow.ui:388
msgid "_Input Devices"
msgstr "_Eingabegeräte"

#: src/mainwindow.ui:416
msgid "<i>No cards available for configuration</i>"
msgstr "<i>Keine Karten zur Konfiguration vorhanden</i>"

#: src/mainwindow.ui:431
msgid "Show volume meters"
msgstr "Tonmesser anzeigen"

#: src/mainwindow.ui:448
msgid "_Configuration"
msgstr "_Konfiguration"

#: src/cardwidget.ui:30
msgid "Card Name"
msgstr "Soundkarten-Name"

#: src/cardwidget.ui:45
#, fuzzy
msgid "Lock card to this profile"
msgstr "Karte an dieses Profil binden"

#: src/cardwidget.ui:66
msgid "<b>Profile:</b>"
msgstr "<b>Profil:</b>"

#: src/cardwidget.ui:82
msgid "<b>Codec:</b>"
msgstr "<b>Codec:</b>"

#: src/channelwidget.ui:9
msgid "<b>left-front</b>"
msgstr "<b>vorne links</b>"

#: src/channelwidget.ui:33
msgid "<small>50%</small>"
msgstr "<small>50%</small>"

#: src/renamedialog.ui:16
msgid "<b>Rename device to:</b>"
msgstr "<b>Gerät umbenennen in:</b>"

#: src/renamedialog.ui:37
msgid "_Cancel"
msgstr "Abbre_chen"

#: src/renamedialog.ui:45
#, fuzzy
msgid "_Ok"
msgstr "_OK"

#: src/streamwidget.ui:34
msgid "Stream Title"
msgstr "Spurtitel"

#: src/streamwidget.ui:44
msgid "direction"
msgstr "Richtung"

#: src/streamwidget.ui:64 src/devicewidget.ui:48
msgid "Mute audio"
msgstr "Stummschalten"

#: src/streamwidget.ui:78 src/devicewidget.ui:62
msgid "Lock channels together"
msgstr "Kanäle zusammen anpassen"

#: src/devicewidget.ui:33
msgid "Device Title"
msgstr "Gerätetitel"

#: src/devicewidget.ui:77
#, fuzzy
msgid "Set as default"
msgstr "Als Ausweichoption setzen"

#: src/devicewidget.ui:95
msgid "<b>Port:</b>"
msgstr "<b>Port:</b>"

#: src/devicewidget.ui:136
msgid "PCM"
msgstr "PCM"

#: src/devicewidget.ui:148
msgid "AC-3"
msgstr "AC-3"

#: src/devicewidget.ui:158
msgid "DTS"
msgstr "DTS"

#: src/devicewidget.ui:168
msgid "E-AC-3"
msgstr "E-AC-3"

#: src/devicewidget.ui:178
msgid "MPEG"
msgstr "MPEG"

#: src/devicewidget.ui:188
msgid "AAC"
msgstr "AAC"

#: src/devicewidget.ui:198
msgid "TrueHD"
msgstr "TrueHD"

#: src/devicewidget.ui:208
msgid "DTS-HD"
msgstr "DTS-HD"

#: src/devicewidget.ui:224
msgid "<b>Latency offset:</b>"
msgstr "<b>Latenz-Offset:</b>"

#: src/devicewidget.ui:237
msgid "ms"
msgstr "ms"

#: src/devicewidget.ui:247
msgid "Advanced"
msgstr "Erweitert"

#: src/pavucontrol.cc:107
#, c-format
msgid "could not read JSON from list-codecs message response: %s"
msgstr ""

#: src/pavucontrol.cc:116
msgid "list-codecs message response is not a JSON array"
msgstr ""

#: src/pavucontrol.cc:164
msgid "list-codecs message response could not be parsed correctly"
msgstr ""

#: src/pavucontrol.cc:184
#, c-format
msgid "could not read JSON from get-codec message response: %s"
msgstr ""

#: src/pavucontrol.cc:193
msgid "get-codec message response is not a JSON value"
msgstr ""

#: src/pavucontrol.cc:201
msgid "could not get codec name from get-codec message response"
msgstr ""

#: src/pavucontrol.cc:223
#, c-format
msgid "could not read JSON from get-profile-sticky message response: %s"
msgstr ""

#: src/pavucontrol.cc:232
msgid "get-profile-sticky message response is not a JSON value"
msgstr ""

#: src/pavucontrol.cc:252 src/cardwidget.cc:153 src/cardwidget.cc:181
#, c-format
msgid "pa_context_send_message_to_object() failed: %s"
msgstr "pa_context_send_message_to_object() fehlgeschlagen: %s"

#: src/pavucontrol.cc:270
#, c-format
msgid "could not read JSON from list-handlers message response: %s"
msgstr ""

#: src/pavucontrol.cc:279
msgid "list-handlers message response is not a JSON array"
msgstr ""

#: src/pavucontrol.cc:327
msgid "list-handlers message response could not be parsed correctly"
msgstr ""

#: src/pavucontrol.cc:361
msgid "Card callback failure"
msgstr "Soundkarten Callback-Fehler"

#: src/pavucontrol.cc:389
msgid "Sink callback failure"
msgstr "Senke Callback-Fehler"

#: src/pavucontrol.cc:413
msgid "Source callback failure"
msgstr "Quellen Callback-Fehler"

#: src/pavucontrol.cc:432
msgid "Sink input callback failure"
msgstr "Senken-Eingabe Callback-Fehler"

#: src/pavucontrol.cc:451
msgid "Source output callback failure"
msgstr "Quellen-Ausgabe Callback-Fehler"

#: src/pavucontrol.cc:481
msgid "Client callback failure"
msgstr "Client Callback-Fehler"

#: src/pavucontrol.cc:497
msgid "Server info callback failure"
msgstr "Server-Information Callback-Fehler"

#: src/pavucontrol.cc:515 src/pavucontrol.cc:812
#, c-format
msgid "Failed to initialize stream_restore extension: %s"
msgstr "Initialisierung der »stream_restore«-Erweiterung gescheitert: %s"

#: src/pavucontrol.cc:533
msgid "pa_ext_stream_restore_read() failed"
msgstr "pa_ext_stream_restore_read() gescheitert"

#: src/pavucontrol.cc:551 src/pavucontrol.cc:826
#, c-format
msgid "Failed to initialize device restore extension: %s"
msgstr ""
"Initialisierung der Gerätewiederherstellung-Erweiterung gescheitert: %s"

#: src/pavucontrol.cc:572
msgid "pa_ext_device_restore_read_sink_formats() failed"
msgstr "pa_ext_device_restore_read_sink_formats() gescheitert"

#: src/pavucontrol.cc:590 src/pavucontrol.cc:839
#, c-format
msgid "Failed to initialize device manager extension: %s"
msgstr "Initialisierung der Gerätemanager-Erweiterung gescheitert: %s"

#: src/pavucontrol.cc:609
msgid "pa_ext_device_manager_read() failed"
msgstr "pa_ext_device_manager_read() gescheitert"

#: src/pavucontrol.cc:626
msgid "pa_context_get_sink_info_by_index() failed"
msgstr "pa_context_get_sink_info_by_index() gescheitert"

#: src/pavucontrol.cc:639
msgid "pa_context_get_source_info_by_index() failed"
msgstr "pa_context_get_source_info_by_index() gescheitert"

#: src/pavucontrol.cc:652 src/pavucontrol.cc:665
msgid "pa_context_get_sink_input_info() failed"
msgstr "pa_context_get_sink_input_info() gescheitert"

#: src/pavucontrol.cc:678
msgid "pa_context_get_client_info() failed"
msgstr "pa_context_get_client_info() gescheitert"

#: src/pavucontrol.cc:688 src/pavucontrol.cc:753
msgid "pa_context_get_server_info() failed"
msgstr "pa_context_get_server_info() gescheitert"

#: src/pavucontrol.cc:701
msgid "pa_context_get_card_info_by_index() failed"
msgstr "pa_context_get_card_info_by_index() gescheitert"

#: src/pavucontrol.cc:744
msgid "pa_context_subscribe() failed"
msgstr "pa_context_subscribe() gescheitert"

#: src/pavucontrol.cc:760
msgid "pa_context_client_info_list() failed"
msgstr "pa_context_client_info_list() gescheitert"

#: src/pavucontrol.cc:767
msgid "pa_context_get_card_info_list() failed"
msgstr "pa_context_get_card_info_list() gescheitert"

#: src/pavucontrol.cc:774
msgid "pa_context_get_sink_info_list() failed"
msgstr "pa_context_get_sink_info_list() gescheitert"

#: src/pavucontrol.cc:781
msgid "pa_context_get_source_info_list() failed"
msgstr "pa_context_get_source_info_list() gescheitert"

#: src/pavucontrol.cc:788
msgid "pa_context_get_sink_input_info_list() failed"
msgstr "pa_context_get_sink_input_info_list() gescheitert"

#: src/pavucontrol.cc:795
msgid "pa_context_get_source_output_info_list() failed"
msgstr "pa_context_get_source_output_info_list() gescheitert"

#: src/pavucontrol.cc:854 src/pavucontrol.cc:905
msgid "Connection failed, attempting reconnect"
msgstr "Verbindung gescheitert, versuche wiederzuverbinden"

#: src/pavucontrol.cc:892
msgid ""
"Connection to PulseAudio failed. Automatic retry in 5s\n"
"\n"
"In this case this is likely because PULSE_SERVER in the Environment/X11 Root "
"Window Properties\n"
"or default-server in client.conf is misconfigured.\n"
"This situation can also arise when PulseAudio crashed and left stale details "
"in the X11 Root Window.\n"
"If this is the case, then PulseAudio should autospawn again, or if this is "
"not configured you should\n"
"run start-pulseaudio-x11 manually."
msgstr ""
"Verbindung zu PulseAudio fehlgeschlagen. Erneuter automatischer Versuch in 5 "
"s.\n"
"\n"
"Dies ist wahrscheinlich durch eine falsche Konfiguration verursacht.\n"
"Entweder die PULSE_SERVER Umgebungsvariable/X11 Root Window Eigenschaft\n"
"oder die default-server Angabe in client.conf sind ungültig.\n"
"Diese Situation kann auch auftreten, wenn PulseAudio abgestürzt ist und\n"
"nicht mehr gültige Details im X11 Root Window hinterlassen hat.\n"
"Wenn das der Fall ist, sollte PulseAudio sich automatisch neustarten.\n"
"Wenn dies nicht eingestellt ist, sollten Sie start-pulseaudio-x11 manuell "
"ausführen."

#: src/cardwidget.cc:126
msgid "pa_context_set_card_profile_by_index() failed"
msgstr "pa_context_set_card_profile_by_index() gescheitert"

#: src/channelwidget.cc:99
#, c-format
msgid "<small>%0.0f%% (%0.2f dB)</small>"
msgstr "<small>%0.0f%% (%0.2f dB)</small>"

#: src/channelwidget.cc:101
#, c-format
msgid "<small>%0.0f%% (-&#8734; dB)</small>"
msgstr "<small>%0.0f%% (-&#8734; dB)</small>"

#: src/channelwidget.cc:104
#, c-format
msgid "%0.0f%%"
msgstr "%0.0f%%"

#: src/channelwidget.cc:139
msgid "<small>Silence</small>"
msgstr "<small>Stumm</small>"

#: src/channelwidget.cc:139
msgid "<small>Min</small>"
msgstr "<small>Minimum</small>"

#: src/channelwidget.cc:141
msgid "<small>100% (0 dB)</small>"
msgstr "<small>100% (0 dB)</small>"

#: src/channelwidget.cc:145
msgid "<small><i>Base</i></small>"
msgstr "<small><i>Basis</i></small>"

#: src/devicewidget.cc:75
msgid "Rename Device..."
msgstr "Gerät umbenennen..."

#: src/devicewidget.cc:179
msgid "pa_context_set_port_latency_offset() failed"
msgstr "pa_context_set_port_latency_offset() gescheitert"

#: src/devicewidget.cc:257
msgid "Sorry, but device renaming is not supported."
msgstr ""
"Entschuldigung, aber das Umbenennen von Geräten wird nicht unterstützt."

#: src/devicewidget.cc:259
msgid ""
"You need to load module-device-manager in the PulseAudio server in order to "
"rename devices"
msgstr ""
"Sie müssen den module-device-manager im PulseAudio Server laden, um Geräte "
"umbenennen zu können"

#: src/devicewidget.cc:294
msgid "pa_ext_device_manager_write() failed"
msgstr "pa_ext_device_manager_write() gescheitert"

#: src/mainwindow.cc:171
#, c-format
msgid "Error reading config file %s: %s"
msgstr "Fehler beim Lesen der Konfigurationsdatei %s: %s"

#: src/mainwindow.cc:245
msgid "Error saving preferences"
msgstr "Fehler beim Speichern der Einstellungen"

#: src/mainwindow.cc:253
#, c-format
msgid "Error writing config file %s"
msgstr "Fehler beim Schreiben der Konfigurationsdatei %s"

#: src/mainwindow.cc:314
msgid " (plugged in)"
msgstr " (eingesteckt)"

#: src/mainwindow.cc:318 src/mainwindow.cc:426
msgid " (unavailable)"
msgstr " (nicht verfügbar)"

#: src/mainwindow.cc:320 src/mainwindow.cc:423
msgid " (unplugged)"
msgstr " (ausgesteckt)"

#: src/mainwindow.cc:625
msgid "Failed to read data from stream"
msgstr "Lesen des Datenstroms gescheitert"

#: src/mainwindow.cc:669
msgid "Peak detect"
msgstr "Ausschlagsserkennung"

#: src/mainwindow.cc:670
msgid "Failed to create monitoring stream"
msgstr "Erstellung des Beobachterdatenstroms gescheitert"

#: src/mainwindow.cc:685
msgid "Failed to connect monitoring stream"
msgstr "Verbindungsaufbau zum Beobachterdatenstrom gescheitert"

#: src/mainwindow.cc:819
msgid ""
"Ignoring sink-input due to it being designated as an event and thus handled "
"by the Event widget"
msgstr ""
"Ignoriere die Sink-Eingabe, da sie als ein Ereignis bezeichnet und daher vom "
"Event-Widget behandelt wurde"

#: src/mainwindow.cc:994
msgid "System Sounds"
msgstr "Systemklänge"

#: src/mainwindow.cc:1340
msgid "Establishing connection to PulseAudio. Please wait..."
msgstr "Baue Verbindung zu PulseAudio auf. Bitte warten..."

#: src/rolewidget.cc:68
msgid "pa_ext_stream_restore_write() failed"
msgstr "pa_ext_stream_restore_write() gescheitert"

#: src/sinkinputwidget.cc:35
msgid "on"
msgstr "ein"

#: src/sinkinputwidget.cc:38
msgid "Terminate Playback"
msgstr "Wiedergabe beenden"

#: src/sinkinputwidget.cc:78
msgid "Unknown output"
msgstr "Unbekannte Ausgabe"

#: src/sinkinputwidget.cc:87
msgid "pa_context_set_sink_input_volume() failed"
msgstr "pa_context_set_sink_input_volume() gescheitert"

#: src/sinkinputwidget.cc:102
msgid "pa_context_set_sink_input_mute() failed"
msgstr "pa_context_set_sink_input_mute() gescheitert"

#: src/sinkinputwidget.cc:112
msgid "pa_context_kill_sink_input() failed"
msgstr "pa_context_kill_sink_input() gescheitert"

#: src/sinkwidget.cc:119
msgid "pa_context_set_sink_volume_by_index() failed"
msgstr "pa_context_set_sink_volume_by_index() gescheitert"

#: src/sinkwidget.cc:137
msgid "Volume Control Feedback Sound"
msgstr "Lautstärkeregler-Rückgabeklang"

#: src/sinkwidget.cc:155
msgid "pa_context_set_sink_mute_by_index() failed"
msgstr "pa_context_set_sink_mute_by_index() gescheitert"

#: src/sinkwidget.cc:169
msgid "pa_context_set_default_sink() failed"
msgstr "pa_context_set_default_sink() gescheitert"

#: src/sinkwidget.cc:189
msgid "pa_context_set_sink_port_by_index() failed"
msgstr "pa_context_set_sink_port_by_index() gescheitert"

#: src/sinkwidget.cc:231
msgid "pa_ext_device_restore_save_sink_formats() failed"
msgstr "pa_ext_device_restore_save_sink_formats() gescheitert"

#: src/sourceoutputwidget.cc:35
msgid "from"
msgstr "von"

#: src/sourceoutputwidget.cc:38
msgid "Terminate Recording"
msgstr "Aufnahme beenden"

#: src/sourceoutputwidget.cc:83
msgid "Unknown input"
msgstr "Unbekannte Eingabe"

#: src/sourceoutputwidget.cc:93
msgid "pa_context_set_source_output_volume() failed"
msgstr "pa_context_set_source_output_volume() gescheitert"

#: src/sourceoutputwidget.cc:108
msgid "pa_context_set_source_output_mute() failed"
msgstr "pa_context_set_source_output_mute() gescheitert"

#: src/sourceoutputwidget.cc:119
msgid "pa_context_kill_source_output() failed"
msgstr "pa_context_kill_source_output() gescheitert"

#: src/sourcewidget.cc:46
msgid "pa_context_set_source_volume_by_index() failed"
msgstr "pa_context_set_source_volume_by_index() gescheitert"

#: src/sourcewidget.cc:61
msgid "pa_context_set_source_mute_by_index() failed"
msgstr "pa_context_set_source_mute_by_index() gescheitert"

#: src/sourcewidget.cc:75
msgid "pa_context_set_default_source() failed"
msgstr "pa_context_set_default_source() gescheitert"

#: src/sourcewidget.cc:97
msgid "pa_context_set_source_port_by_index() failed"
msgstr "pa_context_set_source_port_by_index() gescheitert"

#: src/pavuapplication.cc:158
msgid "Select a specific tab on load."
msgstr "Spezifischen Tab beim Laden auswählen."

#: src/pavuapplication.cc:159
msgid "number"
msgstr "Nummer"

#: src/pavuapplication.cc:164
msgid "Retry forever if pa quits (every 5 seconds)."
msgstr ""
"Immer wieder erneut versuchen, wenn PulseAudio sich beendet (alle 5 "
"Sekunden)."

#: src/pavuapplication.cc:169
msgid "Maximize the window."
msgstr "Fenster maximieren."

#: src/pavuapplication.cc:174
msgid "Show version."
msgstr "Version anzeigen."

#~ msgid "multimedia-volume-control"
#~ msgstr "Multimedia-Lautstärkeregler"

#~ msgid "Terminate"
#~ msgstr "Beenden"

#~ msgid "window2"
#~ msgstr "Fenster2"

#~ msgid "window1"
#~ msgstr "Fenster1"

#~ msgid "Set as fallback"
#~ msgstr "Als Ausweichoption setzen"

#~ msgid "_OK"
#~ msgstr "_OK"

#~ msgid "AC3"
#~ msgstr "AC3"

#~ msgid "EAC3"
#~ msgstr "EAC3"

#~ msgid "Device"
#~ msgstr "Gerät"

#~ msgid "pa_context_move_sink_input_by_index() failed"
#~ msgstr "pa_context_move_sink_input_by_index() gescheitert"

#~ msgid "pa_context_move_source_output_by_index() failed"
#~ msgstr "pa_context_move_source_output_by_index() gescheitert"
